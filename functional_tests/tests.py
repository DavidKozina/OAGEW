from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase, RequestFactory
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os

from polls.views import login

class SimpleTest(TestCase):
    def setUp(self):
        self.server_url = "http://localhost:8000/"
            
        firefox_capabilities = DesiredCapabilities.FIREFOX
        firefox_capabilities['marionette'] = True
        firefox_capabilities['binary'] = '/usr/bin/firefox'
        self.browser = webdriver.Firefox(capabilities=firefox_capabilities)
        self.browser.implicitly_wait(3)
        
    def create_user():
        self.user = User.objects.create_user(
            username='david', email='david@…', password='top_secret')
        
    def test_open(self):
        self.browser.get(self.server_url)
        self.assertIn(self.server_url + "/login/",self.browser.current_url)
