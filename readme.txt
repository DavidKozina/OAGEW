
For using email, create file "adm_settings.py" in OAGEW/OAGEW directory and copy into this:
#--email-settings-----------------------------------
EMAIL_HOST = ''
EMAIL_HOST_USER = '' 
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = #port-number
DEFAULT_FROM_EMAIL = ""
EMAIL_USE_TLS = True
#---------------------------------------------------
