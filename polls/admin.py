from django.contrib import admin
from .models import Profile, Game, Dialog, Frame, Item, Gameplay

class DialogInline(admin.TabularInline):
    model = Dialog
    extra = 0
class FrameInline(admin.TabularInline):
    model = Frame
    extra = 0
class ItemsInline(admin.TabularInline):
    model = Item
    extra = 0

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass#readonly_fields = ('user', )
    
@admin.register(Gameplay)
class GameRateAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Game)   
class GameAdmin(admin.ModelAdmin):
    model = Game
    readonly_fields = ('name', 'description', 'playable', 'author')
    inlines = [
        FrameInline,
        DialogInline,
        ItemsInline,
    ]
    
