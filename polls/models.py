from __future__ import unicode_literals
from enum import Enum
from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.core.files.storage import FileSystemStorage
from django.core.validators import MaxValueValidator, MinValueValidator
import os


fs = FileSystemStorage(location='media/')

class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)
        
class Gender_enum(ChoiceEnum):
    Female = 1
    Male = 2
    Neither = 3

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE, null=False)
    gender = models.CharField(max_length=10, choices = Gender_enum.choices(), default = Gender_enum.Neither)
    followers = models.ManyToManyField(User, related_name="favourites", blank = True)
    
    def __str__(self):
        return "Profile: " + str(self.user)
        
    def get_rating(self):
        output = 0
        num_games = 0;
        for g in self.games.filter(playable = True):
            rating = g.get_rating()
            if rating > 0:
                num_games += 1
                output += rating
        if num_games == 0: return 0
        return float(output / num_games)

class Image(models.Model):
    author = models.ForeignKey(Profile, null=False)
    description = models.CharField(max_length = 100, null=False)
    photo = models.ImageField(upload_to = 'images')
      
class Game(models.Model):
    author = models.ForeignKey(Profile, null=False, related_name="games")
    name = models.CharField(max_length = 64, null=False)
    description = models.CharField(max_length = 300, null = True, blank = True)
    playable = models.BooleanField(default=False)
    players = models.ManyToManyField(User, related_name="marked_games")
    
    def __str__(self):
        return "Game by " + self.author.user.username + ": " + self.name
        
    def get_rating(self):
        if self.saves.all().count() == 0: return 0
        output = 0
        num_rats = 0
        for r in self.saves.all():
            if (r.rating) != 0:
                num_rats += 1
                output += (r.rating)*20
        if num_rats == 0: return 0
        return float(output / num_rats)
    
class Dialog(models.Model):
    game = models.ForeignKey(Game, on_delete = models.CASCADE, null=False)
    data = models.FileField(upload_to = 'dialogs', null=False, storage=fs)
    name = models.CharField(max_length = 20, null=False, default="unnamed")
    
    def __str__(self):
        return self.name
        
    def get_file(self):
        print("RD:\n" + self.data.storage.open(self.data.name, "r").read())
        return self.data.storage.open(self.data.name, "r").read()
        
    def write(self, data):
        print("WR:\n" + data)
        f = self.data.storage.open(self.data.name, "w")
        f.write(data)
        f.close()
        
@receiver(models.signals.post_delete, sender=Dialog)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.data:
        if os.path.isfile(instance.data.path):
            os.remove(instance.data.path)
        
    
class Frame(models.Model):
    game = models.ForeignKey(Game, on_delete = models.CASCADE, null=False)
    start_dialog = models.ForeignKey(Dialog, on_delete = models.CASCADE, null=True, blank = True,)
    name = models.CharField(max_length = 20, null=False, default="unnamed")
    notes = models.CharField(max_length = 100, default="", blank = True,)
    
    def __str__(self):
        return self.name
    
class Item(models.Model):
    game = models.ForeignKey(Game, on_delete = models.CASCADE, null=False)
    name = models.CharField(max_length = 20, null=False, default="unnamed")
    
    def __str__(self):
        return self.name
        
class Gameplay(models.Model):
    game = models.ForeignKey(Game, related_name="saves", default = None)
    user = models.ForeignKey(User, related_name="rated_games", default = None)
    rating = models.SmallIntegerField(null=False, default = 0, 
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ])
    dialog = models.ForeignKey(Dialog, default = None, blank = True, null=True)
    frame = models.ForeignKey(Frame, default = None, blank = True, null=True)
    sentence = models.SmallIntegerField(null=False, default = 0)
    flag_list = models.TextField(default="")
    item_list = models.TextField(default="")
    comment = models.TextField(default="")

    def __str__(self):
        return self.user.username + "->" + self.game.author.user.username + ":" + self.game.name + " " + "*"*self.rating
