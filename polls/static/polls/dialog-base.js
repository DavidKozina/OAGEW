    var data = "";
    var number = 0;  
    var sentId = 0;
    var sentences = [];
    var textDiv = document.getElementById("text");
    var choicesDiv = document.getElementById("choices");
    var conditionDiv = document.getElementById("condition");
    var effectDiv = document.getElementById("effect");
    var numberSel = document.getElementById("number");
    var elseSel = document.getElementById("else");
    var items = {};
    var flags = [];
    var sentence = null;
    var textFile = null;
    var filename = "";
    var editor = true;
    var dialogname = "";
    var loadedItems = {"nothing":1};
    var loadedFlags = [];

class Choice{
    constructor(text, ciel, condition = function(){return true}) {
        this.text = text;
        this.ciel = ciel;
        this.condition = condition;
    }
}
class Sentence{
    constructor(number, condition , elseSent, text, choices, extra) {
        this.text = text;
        this.choices = choices;
        this.number = number;
        this.condition = condition;
        this.extra = extra;
        this.first = true;
        this.elseSent = elseSent;
    }
    show(){
        textDiv.innerHTML = this.text;
        var choicesToDisplay = "";
        this.first = false;
        for(var i = 0; i < this.choices.length; i++){
            if ((this.choices[i].condition == "") || (Fce.exe(this.choices[i].condition))){
                choicesToDisplay += "<button class='choice' onClick='changeSent("+this.choices[i].ciel+")'>"+this.choices[i].text + "</button><br />";
            }
        }

        choicesDiv.innerHTML = choicesToDisplay;
        Fce.exe(this.extra);
        showInventory();
        sentence = this;
    }
    showEdit(toSave = false){
        if (toSave) save();
        textDiv.value  = this.text;
        effectDiv.value = Fce.encrypt(this.extra);
        conditionDiv.value = Fce.encrypt(this.condition);
        setSelect(numberSel, this.number);
        setSelect(elseSel, this.elseSent);
        document.getElementById("elseText").innerHTML = findSent(elseSel.value).text;
        var choicesToDisplay = "<table style='width:100%; border: solid black 1px'>";
        this.first = false;
        for(var i = 0; i < this.choices.length; i++){
            choicesToDisplay += "<tr class='editor'>"
            choicesToDisplay += '<td style="width:5%"><select id="cielCh:'+i+'" value="'+this.choices[i].ciel+'"onchange="changeSelect('+i+')">'+this.choices[i].ciel+'</select></td>';
            choicesToDisplay += '<td style="width:5%"><button id="cielChB:'+i+'" onClick="changeSent(sentById('+i+'))">Go</button></td>';
            choicesToDisplay += '<td style="width:20%" ><textarea id="cielCon:'+i+'" rows="1" placeholder="condition" onChange="save()">'+Fce.encrypt(this.choices[i].condition)+'</textarea></td>';
            choicesToDisplay += '<td style="width:70%"><textarea id="cielTxt:'+i+'" name="nameCh" rows="1" placeholder="text" style="width:100%" onchange="save()">'+this.choices[i].text+'</textarea></td>';
            choicesToDisplay += '<td><button id="cielDel:'+i+'" onClick="deleteChoice('+i+')">Delete</button><td>';
            choicesToDisplay += '<tr class="editor"><td style="width:100%" colspan="40"><div id="cielSent:'+i+'" class="editor">'+findSent(this.choices[i].ciel).text+'</div></td></tr>';
            choicesToDisplay += "</tr>"
        }
        choicesDiv.innerHTML = choicesToDisplay + "</table>";
        for(var i = 0; i < this.choices.length; i++){
            setSelect(document.getElementById("cielCh:"+i.toString()), this.choices[i].ciel);
        }
        sentence = this;
    }
    first(){
        return this.first;
    }
}

function showInventory(){
    var inventoryText = "";
    for (var key in items) {
        if (items.hasOwnProperty(key))
            inventoryText += key + ((items[key] > 1) ? (" : " + items[key] + "x") : "") + "<br/>" 
    }
    document.getElementById("inventory").innerHTML = inventoryText
}
    
function createView(div ,isEditor, file, fileOutput = null, startSent = 0, loaded = false){
    if (isEditor){
        div.innerHTML = `
        <table class="editor">
            <tr class="editor">
                <td>
                    Sentence:
                </td>
                <td class="editor">
                    <select id="number" style="width:100%" onChange="changeSent(selectValue(this), true)">num</select>
                </td>
            </tr>
            <tr class="editor">
                <td>
                    Condition: 
                </td>
                <td class="editor">
                    <textarea id="condition" name="text" placeholder="Condition for redirect" onChange="save()" rows="1" style="width:95%"></textarea>
                </td>
                <td class="editor">
                    Else:
                    <select id="else" style="width:100%" onChange="changeElse()">num</select>
                </td>
            </tr>
            <tr class="editor">
                <td>
                <a>functions</a>
                    <div class="profile-on-hover">
                        <table>
                            <tr>
                                <th class="editTable">command</th><th class="editTable">variables</th><th class="editTable">effect</th>
                            </tr>
                            <tr>
                                <td class="editTable">set, setFlag, set_flag</td><td class="editTable">name of flag</td><td class="editTable">turn flag on</td>
                            </tr>
                            <tr>
                                <td class="editTable">reset, resetFlag, reset_flag</td><td class="editTable">name of flag</td><td class="editTable">turn flag off</td>
                            </tr>
                            <tr>
                                <td class="editTable">switch, switchFlag, switch_flag</td><td class="editTable">name of flag</td><td class="editTable">turn flag on</td>
                            </tr>
                            <tr>
                                <td class="editTable">dialog</td><td class="editTable">name of dialog</td><td class="editTable">change dialog</td>
                            </tr>
                            <tr>
                                <td class="editTable">getItem, get_item</td><td class="editTable">name of item</td><td class="editTable">give one item</td>
                            </tr>
                            <tr>
                                <td class="editTable">get, getItems, get_items</td><td class="editTable">name of item, count</td><td class="editTable">give some items</td>
                            </tr>
                            <tr>
                                <td class="editTable">putItem, put_item</td><td class="editTable">name of item</td><td class="editTable">take one item</td>
                            </tr>
                            <tr>
                                <td class="editTable">put, putItems, put_items</td><td class="editTable">name of item, count</td><td class="editTable">take some items</td>
                            </tr>
                            <tr>
                                <td class="editTable" colspan="3"><hr></td>
                            </tr>
                            <tr>
                                <td class="editTable">first</td><td class="editTable">nothing</td><td class="editTable">Is user in this sentence at first time?</td>
                            </tr>
                            <tr>
                                <td class="editTable">item</td><td class="editTable">name of item</td><td class="editTable">Have at least one piece of item?</td>
                            </tr>
                            <tr>
                                <td class="editTable">item</td><td class="editTable">name of item, count</td><td class="editTable">Have at least some pieces of items?</td>
                            </tr>
                            <tr>
                                <td class="editTable">flag</td><td class="editTable">name of flag</td><td class="editTable">Is flag set on?</td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class="editor">
                    <div id="elseText"></div>
                </td> 
            </tr>
            <tr class="editor">
                <td colspan="2">
                    <textarea id="text" name="text" rows="10" placeholder="Text of statement" onChange="save()" style="width:95%"></textarea>
                </td>
            </tr>
            <tr class="editor">
                <td>
                    Effect:
                </td>
                <td class="editor">
                    <textarea id="effect" name="text" rows="1" placeholder="effect of statement" onChange="save()" style="width:95%"></textarea>
                </td>
            </tr>
            <tr class="editor">
                <td colspan="3">
                    <div id="choices"></div>
                </td>
            </tr>
            <tr class="editor">
                <td colspan="3">
                    <button id = "newChoice" onClick="newChoice()">Add Choice</button>
                </td>
            </tr>
        </table>
        `
    }else{
        div.innerHTML =  `
            <div id="text" class="gametext">  </div>
            <div id="choices" class="choice">  </div>
            <bold>Inventory:</bold>
            <div id="inventory" class="inventory">  </div>
        `
    }
    data = "";
    number = 0;  
    sentId = 0;
    sentences = [];
    textDiv = document.getElementById("text");
    choicesDiv = document.getElementById("choices");
    conditionDiv = document.getElementById("condition");
    effectDiv = document.getElementById("effect");
    numberSel = document.getElementById("number");
    elseSel = document.getElementById("else");
    fileInput = fileOutput;
    items = {};
    flags = [];
    sentence = null;
    textFile = null;
    editor = isEditor;
    if (editor) {
        encrypt(file);
        if (sentences.length == 0) sentences.push(new Sentence(0, "" , 0, "", [], ""));
        sentences[0].showEdit();
        setSelect(numberSel, "0");
        setSelect(elseSel, "0");
        save();
    }else{
        showDialog(file, startSent, loaded);
        dialogname = file;
    }
}

function deleteChoice(index){
    delete sentence.choices[index];
    sentence.showEdit(true);
}

function sentById(id){
   return selectValue(document.getElementById("cielCh:"+id.toString()));
}

function changeSelect(id){
    selectSel(document.getElementById("cielCh:"+id.toString()));
    document.getElementById("cielSent:"+id.toString()).innerHTML = findSent(selectValue(document.getElementById("cielCh:"+id.toString()))).text;
    save();
}
function selectSel(select){
    if (select.value !== selectValue(select)) {
        element = document.createElement("option");
        element.text = selectValue(select).toString();
        select.add(element);
        select.value = selectValue(select);
    }
}

function isFlag(flagname){
    for(var i = 0; i < flags.length; i++){
        if (flags[i]==flagname){
            return true;
        }
    }
    return false;
}

function setFlag(flagname){
    for(var i = 0; i < flags.length; i++){
        if (flags[i]==flagname){
            return true;
        }
    }
    flags.push(flagname);
}
function resetFlag(flagname){
    var newFlags = [];
    for(var i = 0; i < flags.length; i++){
        if (flags[i]!=flagname){
            newFlags.push(flags[i]);
        }
    }
    flags = newFlags;
}
function chance(percent){
    return (Math.random()*100 >= percent);
}

function changeElse(select){
    sentence.elseSent = selectValue(select);
    sentence.showEdit(true);
    save();
}

function findSent(number){
    for(var i = 0; i < sentences.length; i++){
        if(sentences[i].number == number){
            if(editor || !Fce.exe(sentences[i].condition)){
                return sentences[i];
            }else return findSent(sentences[i].elseSent);
        }
    }
    if (editor){
        return newSentence();
    }
    textDiv.innerHTML = "<span class='error'>Its sad, but this is an ERROR :( </span><br / > Error appears on sentence number "+number;
    choicesDiv.innerHTML = "";
}
function changeSent(number){
    console.log("change: " + number)
    if (editor){ 
        save();
        findSent(number).showEdit(true); 
    }else{
        findSent(number).show();
    }
    lastSent = number;
}
  
function save(){
    sentence.text = textDiv.value;
    sentence.elseSent = selectValue(elseSel);
    sentence.condition = Fce.crypt(conditionDiv.value);
    sentence.extra = Fce.crypt(effectDiv.value);
    for(var i = 0; i < sentence.choices.length; i++){
        if(sentence.choices[i] == undefined){
            sentence.choices.splice(i, 1);
            continue;
        }
        sentence.choices[i].text = document.getElementById("cielTxt:"+i).value;
        sentence.choices[i].ciel = selectValue(document.getElementById("cielCh:"+i));
        sentence.choices[i].condition = Fce.crypt(document.getElementById("cielCon:"+i).value);
    }
    saveToFile(crypt(sentences));
    sentence.showEdit();
}

function saveToFile(content) {
    fileInput.value = content;
}

function readSingleFile(file) {
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    filename = document.getElementById('upload').files[0].name;
    encrypt(contents);
    if (editor) {sentences[0].showEdit();}else{ sentences[0].show();}
    if (editor) setSelect(numberSel, "0");
    if (editor) setSelect(elseSel, "0");
  };
  reader.readAsText(file);
}
function setSelect(select, defaultValue){
    select.innerHTML = "";
    choiceSetNumbers = searchSentences();
    for(var i = 0; i < choiceSetNumbers.length; i++){
        element = document.createElement("option");
        element.text = choiceSetNumbers[i].toString();
        select.add(element); 
    }
    element = document.createElement("option");
    element.text = "New";
    select.add(element); 
    select.value = defaultValue;
}
function selectValue(select){
    if(select.value == "New"){
        return getFreeSentNumber();
    }else{
        return select.value;
    }
}
function searchSentences(){
    var output = [];
    for(var i = 0; i < sentences.length; i++){
        if(!output.includes(sentences[i].number)){
            output.push(sentences[i].number);
        }
    }
    return output;
}
function getFreeSentNumber(){
    var numbers = searchSentences();
    var testNum = -1;
    var canBe = false;
    while (!canBe){ 
        testNum += 1;
        canBe = true;
        for(var i = 0; i < numbers.length; i++){
            if(testNum == numbers[i]){
                canBe = false;
                break;
            }
        }
    }
    return testNum;
}
function newChoice(){
    save();
    sentence.choices.push(new Choice("", 0, ""));
    sentence.showEdit();
}
function newSentence(){
    sentences.push(new Sentence(getFreeSentNumber(),"", "", "", [], ""));
    sentence.showEdit(true);
    return sentences[sentences.length-1];
}
function changeElse(){
    if (elseSel.value !== selectValue(elseSel)) {
        var value = selectValue(elseSel);
        element = document.createElement("option");
        element.text = selectValue(elseSel).toString();
        elseSel.options[elseSel.options.length - 1] = element;
        
        element = document.createElement("option");
        element.text = "New";
        elseSel.add(element);
        elseSel.value = value;
    }
    sentence.elseSent = selectValue(elseSel);
    document.getElementById("elseText").innerHTML = findSent(elseSel.value, true).text;
}
function haveItem(itemname, count = 1){
    if (typeof items[itemname] !== 'undefined'){
        if (items[itemname] >= count) return true;
    }
    return false;
}
function getItem(itemname, count = 1){
    if (typeof items[itemname] !== 'undefined'){
        items[itemname] += count;
        if (items[itemname] < 0) items[itemname] = 0;
    }else{
        items[itemname] = count;
    }
}    

function crypt(data){
    var output = "";
    for(i = 0; i < data.length; i++){
        output += "{";
        output += data[i].number + "|";
        output += data[i].condition + "|";
        output += data[i].elseSent + "|";
        output += data[i].text + "|";
        output += data[i].extra + "|";
        for(k = 0; k < data[i].choices.length; k++){
            output += data[i].choices[k].condition + "|";
            output += data[i].choices[k].ciel + "|";
            output += data[i].choices[k].text + "|";
        }
        output += "}";
    }
    return output;
}
function encrypt(data){
    var mode = "";
    var content = "";
    var sentence = null;
    sentences = [];
    var specialChar = false;
    var choice = null;
    for(i = 0; i < data.length; i++){
        if (!specialChar){
            if (data[i]=="{"){
                sentence = new Sentence(-1, null, null, -1, [])
                content = "";
                mode = "number"; 
            }else if (data[i]=="|" && mode=="number"){
                sentence.number = parseInt(content);
                content = "";
                mode = "condition";
            }else if (data[i]=="|" && mode=="condition"){
                sentence.condition = content;
                content = "";
                mode = "else";
            }else if (data[i]=="|" && mode=="else"){
                sentence.elseSent = content;
                content = "";
                mode = "text"; 
            }else if (data[i]=="|" && mode=="text"){
                sentence.text = content;
                content = "";
                mode = "effect";
            }else if (data[i]=="|" && mode=="effect"){
                sentence.extra = content;
                content = "";
                mode = "choiceCon";
            }else if (data[i]=="|" && mode=="choiceCon"){
                choice = new Choice(null, null, null);
                choice.condition = content;
                content = "";
                mode = "choiceCiel";
            }else if (data[i]=="|" && mode=="choiceCiel"){
                choice.ciel = content;
                content = "";
                mode = "choiceText";
            }else if (data[i]=="|" && mode=="choiceText"){
                choice.text = content;
                sentence.choices.push(choice);
                content = "";
                mode = "choiceCon";
            }else if (data[i]=="}"){
                sentences.push(sentence);
                mode = "end";
            }else if (data[i]=="\\"){
                specialChar = true;
            }else{
                content += data[i];
            }
        }else{
            content += data[i];
            specialChar = false;
        }
    }
}
function showDialog(dialogname, sent = 0, loaded = false){
    waitMessage();
    var xmlhttp;
    if (window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else{
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            showMessage(true, "Dialog loaded.");
            var data = JSON.parse(this.responseText);
            encrypt(data["dialog"]);
            changeSent(sent);
            if (loaded){
                console.log(loadedItems)
                items = loadedItems;
                flags = loadedFlags;
                showInventory();
            }
        }
    }
    xmlhttp.open("GET","get_dialog/"+dialogname,true);
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.send();
}

var fce_def = {
    //effect
    setFlag: 1,
    set_flag: 1,
    set: 1,
    resetFlag: 2,
    reset_flag: 2,
    reset: 2,
    switchFlag: 3,
    switch_flag: 3,
    switch: 3,
    picture: 4,
    frame: 5,
    dialog: 6,
    getItem: 7,
    get_item: 7,
    getItems: 8,
    get_items: 8,
    get: 8,
    putItem: 9,
    put_item: 9,
    putItems: 10,
    put_items: 10,
    put: 10,
    //conditional
    first: 11,
    item: 12,
    items: 13,
    flag: 14,

    }


class Fce{ 
    static crypt(text){
        var toSave = "";
        var statements = text.trim().split(";")
        
        for(var i = 0; i < statements.length; i++){
            if(i != 0) toSave += ";";
            var elements = statements[i].trim().split(" ");
            if(elements.length > 1){
                toSave += fce_def[elements[0]];
                toSave += " " + elements[1];
                switch (fce_def[elements[0]]){
                    case 8:
                    case 10:
                    case 13:
                        toSave += " " + elements[2];
                }
            }else{
                toSave += elements[0];
            }
        }
        return toSave;
    }
    static encrypt(data){
        var statements = data.trim().split(";")
        var output = "";
        for(var i = 0; i < statements.length; i++){
            if(i != 0) output += "; ";
            var elements = statements[i].trim().split(" ");
            if(elements.length > 1){
                var object = Object.keys(fce_def).filter(function(key) {
                        return fce_def[key] == elements[0];
                    })[0]
                output += (object != undefined) ? object : "";
                output += " " + elements[1] + ((elements.length > 2)? (" " + elements[2]) : "" );
            }else{
                var object = Object.keys(fce_def).filter(function(key) {
                        return fce_def[key] == elements[0];
                    })[0]
                output += (object != undefined) ? object : "";
            }
        }
        return output;
    }
    static exe(condition){
        if (condition == "") return false; 
        var statements = condition.trim().split(";");
        var output = true;
        for(var i = 0; i < statements.length; i++){
            var elements = statements[i].split(" ");
            if (elements[0] != ""){
                switch(elements[0]){
                    case "1":
                        setFlag(elements[1]);
                        break;
                    case "2":
                        resetFlag(elements[1]);
                        break;
                    case "3":
                        if(isFlag(elements[1])){ resetFlag(elements[1]);
                            }else{ setFlag(elements[1]);};
                        break;
                    case "4":
                    
                        break;
                    case "5":
                    
                        break;
                    case "6":
                        showDialog(elements[1]);
                        break;
                    case "7":
                        getItem(elements[1]);
                        break;
                    case "8":
                        getItem(elements[1], elements[2]);
                        break;
                    case "9":
                        getItem(elements[1], -1);
                        break;
                    case "10":
                        getItem(elements[1], -elements[2]);
                        break;
                    case "11":
                        output = sentence.first();
                        break;
                    case "12":
                        output = haveItem(elements[1]);
                        break;
                    case "13":
                        output = haveItem(elements[1], elements[2]);
                        break;
                    case "14":
                        output = isFlag(elements[1]);
                        break;
                    default:
                        console.log("ERROR");
                }
            }
        }
        return output;
    }
}
