function waitMessage(){
    try {
        document.getElementById("messageDiv").className = "alert alert-warning";
        document.getElementById("message").innerHTML = "waiting for server...";
        document.getElementById("messageDiv").style.visibility = "visible"; 
    }catch(e){}
}
function showMessage(status,text){
    if (text!=""){
        if (status){
            document.getElementById("messageDiv").className = "alert alert-info";
        }else{
            document.getElementById("messageDiv").className = "alert alert-danger";
        }
        document.getElementById("message").innerHTML = text;
        document.getElementById("messageDiv").style.visibility = "visible"; 
    }else{
        document.getElementById("messageDiv").style.visibility = "hidden"; 
    }
}
