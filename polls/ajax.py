from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import Profile, Game, Frame, Dialog, Gameplay
from django.contrib.auth.models import User
from .utils import Status_message, create_new_frame, create_new_dialog, auth
import json

@login_required(login_url='/login/')
def change_game_playability(request, username, gamename):
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)
    game.playable = not game.playable
    print("!" + game.name+" has changed playability to" + str(game.playable))
    game.save();
    data = {
        'playability': game.playable,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def new_frame(request, username, gamename, framename = ""):
    if not auth(request, username): return JsonResponse("", safe = False)
    if framename != "":
        message = Status_message("New frame created.",True)
        user = User.objects.get(username=username)
        game = Game.objects.get(author = user.profile, name = gamename)
        frames = Frame.objects.filter(game = game)
        for frame in frames:
            if frame.name == framename: message.set("This frame already exists.", False)
        if message.status:
            new_Frame = create_new_frame(game = game, name = framename, start_dialog = None)
    else:
        message = Status_message("You've forgoten to write a name.",False)
    data = {
        'message_text': message.value,
        'message_status': message.status,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def get_frame(request, username, gamename, framename):
    if not auth(request, username): return JsonResponse("", safe = False)
    message = Status_message("Frame loaded.",True)
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)
    frame = Frame.objects.get(game = game, name = framename)
    data = {
        'start_dialog' : frame.start_dialog.name if frame.start_dialog != None else "None",
        'notes' : frame.notes,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')    
def delete_frame(request, username, gamename, framename):
    if not auth(request, username): return JsonResponse("", safe = False)
    message = Status_message("Frame deleted.",True)
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)

    Frame.objects.get(game = game, name = framename).delete()

    data = {
        'message_text': message.value,
        'message_status': message.status,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def set_frame(request, username, gamename, framename, start_dialog, notes):
    if not auth(request, username): return JsonResponse("", safe = False)
    message = Status_message("Frame not saved.", False)
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)
    frame = Frame.objects.get(game = game, name = framename)
    if start_dialog != "None":
        dialog = Dialog.objects.get(game = game, name = start_dialog)
        frame.start_dialog = dialog
    else: frame.start_dialog = None
    frame.notes = notes
    frame.save()

    return JsonResponse("", safe = False)

@login_required(login_url='/login/')
def new_dialog(request, username, gamename, dialogname = ""):
    if not auth(request, username): return JsonResponse("", safe = False)
    print("dialogname: "+dialogname)
    if dialogname != "":
        message = Status_message("New dialog created.",True)
        user = User.objects.get(username=username)
        game = Game.objects.get(author = user.profile, name = gamename)
        dialogs = Dialog.objects.filter(game = game)
        for dialog in dialogs:
            if dialog.name == dialogname: message.set("This dialog already exists.", False)
        if message.status:
            new_Dialog = create_new_dialog(game, dialogname)
    else:
        message = Status_message("You've forgoten to write a name.",False)
    data = {
        'message_text': message.value,
        'message_status': message.status,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def edit_dialog(request, username, gamename, dialogname, author = None):
    if not auth(request, username): return JsonResponse("", safe = False)
    if (author != None): username = author;
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)
    dialog = Dialog.objects.get(game = game, name = dialogname)
    data = {
        "dialog": dialog.get_file(),
    }
    return JsonResponse(data)
    
@login_required(login_url='/login/')
def search_users(request, username, wanted):
    if not auth(request, username): return JsonResponse("", safe = False)
    user = User.objects.get(username=username)
    favouritesQuery = user.favourites.all()
    usersQuery = User.objects.exclude(profile__followers = user).exclude(username = user.username).filter(username__contains = wanted)
    usernames = []
    favnames = []
    for p in favouritesQuery:
        favnames.append({
            "name": p.user.username,
            "rating": p.get_rating(),
            "num_followers": p.followers.all().count(),
        })
    for u in usersQuery:
        usernames.append({
            "name": u.username,
            "rating": u.profile.get_rating(),
            "num_followers": u.profile.followers.all().count(),
        })

    data = {
        'favs': favnames,
        'users': usernames,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def search_games(request, username):
    if not auth(request, username): return JsonResponse("", safe = False)
    user = User.objects.get(username=username)
    favouritesQuery = user.favourites.all()
    games=[]
    for fairy in user.favourites.all().extra(order_by = ['user__username']):
        for game in fairy.games.filter(playable=True).extra(order_by = ['name']):
            marked = game.players.filter(username = user.username).count() == 1
            send_game = {
                'author': game.author.user.username,
                'name': game.name,
                'desc': game.description, 
                'marked': marked,
                'rating': game.get_rating(),
            }
            games.append(send_game)

    data = {
        'games': games,
    }
    return JsonResponse(data)
    
@login_required(login_url='/login/')
def follow(request, username, user_to_follow, status):
    if not auth(request, username): return JsonResponse("", safe = False)
    user = User.objects.get(username=username)
    user_followed =  User.objects.get(username=user_to_follow)
    if status:
        user_followed.profile.followers.add(user)
    else:
        user_followed.profile.followers.remove(user)
    user_followed.save()
    data = {
        'status': True,
    }
    return JsonResponse(data)
    
@login_required(login_url='/login/')
def mark(request, username, author, gamename, status):
    if not auth(request, username): return JsonResponse("", safe = False)
    user = User.objects.get(username=username)
    game_to_mark =  Game.objects.get(author__user__username=author, name=gamename)
    if status:
        game_to_mark.players.add(user)
    else:
        game_to_mark.players.remove(user)
    game_to_mark.save()
    data = {
        'status': True,
    }
    return JsonResponse(data)
    
@login_required(login_url='/login/')
def rate(request, username, author, gamename, value):
    if not auth(request, username): return JsonResponse("", safe = False)
    value =  min(5, int(value))
    user = User.objects.get(username=username)
    game_to_rate =  Game.objects.get(author__user__username=author, name=gamename)
    rates = Gameplay.objects.filter(game=game_to_rate, user = user)
    if value > 0:
        if rates.count() == 1:
            rates.update(rating = value)
        else:
            Game_rate.objects.create(game=game_to_rate, user = user, rating = value).save()
    else:
        if rates.count() == 1: rates[0].delete()
    data = {
        'status': True,
    }
    return JsonResponse(data)

@login_required(login_url='/login/')
def get_comments(request, username, author, gamename):
    if not auth(request, username): return JsonResponse("", safe = False)
    game = Game.objects.get(author__user__username = author, name = gamename)
    gameplays = Gameplay.objects.filter(game = game).order_by("rating")
    comments = []
    for gp in gameplays:
        comment = {
            'user': gp.user.username,
            'rating': gp.rating,
            'text': gp.comment,
        }
        comments.append(comment)
    data = {
        'comments': comments,
    }
    return JsonResponse(data)
