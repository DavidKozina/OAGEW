from django.conf.urls import url
from django.contrib import admin

from . import views, ajax

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #editGamePage
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/new_frame/(?P<framename>[\w.@+-_ ]+)$', ajax.new_frame, name=u'new_frame'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/new_frame/$', ajax.new_frame, name=u'new_frame'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/new_dialog/(?P<dialogname>[\w.@+-_ ]+)$', ajax.new_dialog, name=u'new_dialog'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/new_dialog/$', ajax.new_dialog, name=u'new_dialog'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/edit_dialog/(?P<dialogname>[\w.@+-_ ]+)$', ajax.edit_dialog, name=u'edit_dialog'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/get_frame/(?P<framename>[\w.@+-_ ]+)$', ajax.get_frame, name=u'get_frame'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/delete_frame/(?P<framename>[\w.@+-_ ]+)$', ajax.delete_frame, name=u'delete_frame'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/set_frame/(?P<framename>[\w.@+-_ ]+)/(?P<start_dialog>[\w.@+-_ ]+)/(?P<notes>[\w.@+-_ ]+)$', ajax.set_frame, name=u'set_frame'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/change_game_playability/$', ajax.change_game_playability, name=u'change_game_playability'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/delete/$', views.delete_game, name=u'delete'),

    #editPage
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/(?P<gamename>[\w.@+-_ ]+)/$', views.editGamePage, name='editGamePage'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/edit/$', views.editPage, name='editPage'),

    #playPage
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/play/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)/$', views.play, {'load':False}),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/load/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)/$', views.play, {'load':True}),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/play/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)/get_dialog/(?P<dialogname>[\w.@+-_ ]+)$', ajax.edit_dialog, name='edit_dialog'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/load/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)/get_dialog/(?P<dialogname>[\w.@+-_ ]+)$', ajax.edit_dialog, name='edit_dialog'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/play$', views.choose_for_play, name='choose_for_play'),

    #explorer
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/search/users/(?P<wanted>[\w.@+-_ ]+)$', ajax.search_users, name='search_users'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/search/games/$', ajax.search_games, name='search_games'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/follow/(?P<user_to_follow>[\w.@+-_ ]+)$', ajax.follow, {'status': True} ),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/unfollow/(?P<user_to_follow>[\w.@+-_ ]+)$', ajax.follow, {'status': False} ),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/mark/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)$', ajax.mark, {'status': True} ),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/unmark/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)$', ajax.mark, {'status': False} ),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/get_comments/(?P<author>[\w.@+-_ ]+)/(?P<gamename>[\w.@+-_ ]+)$', ajax.get_comments, name="get_comments"),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/explorer/$', views.explore, name='explore'),
    
    url(r'^$', views.loginPage, name='loginPage'),
    url(r'^login/$', views.loginPage, name='loginPage'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/draw/$', views.drawPage, name='drawPage'),
    url(r'^user_page/(?P<username>[\w.@+-_ ]+)/$', views.userPage, name='userPage'),
    url(r'^init_page/(?P<key>[\w.@+-_ ]+)/$', views.init_page, name='init_page'),
]
