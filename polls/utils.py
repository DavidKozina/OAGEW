from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import Profile, Game, Dialog, Frame
from django.core.files.base import ContentFile


class Status_message():
    def __init__(self, text = "", status = None):
        self.set(text, status)
    def set(self, text, status):
        self.value = text
        self.status = status
    def __str__(self):
        return self.value

def auth(request, username):
    if request.user.get_username() == username:
        return True
    else:
        return False

def find_new_user(request, key):
    users = User.objects.all();
    user = None
    for u in users:
        user = authenticate(request, username=u.username, password=key)
        if user is not None:
            if not user.username[:9] == "new_user:":
                user = None
    return user

def create_full_user(user, username, password, gender):
    print("Creating new user: " + username)
    user.username = username
    user.set_password(password)
    user.save()
    new_profil = Profile.objects.create(user = user, gender = gender)
    new_profil.save()

def test_username(username):
    if username != "":
        message = Status_message(status = True)
        try:
            test_user = User.objects.get(username = username)
        except:
            test_user = None
        if test_user is None:
            if username[:9] == "new_user:":
                message.set("Username can't start with 'new_user'", False) 
        else:
            message.set("User with this username already exists, please use another one.", False)
    else:
        message.set("Username must be longer.", False)
    return message

def create_new_game(profile, name, description):
    message = Status_message(status = True, text="Game created: " + name)
    test_game = Game.objects.filter(author=profile, name=name)
    if len(name) == 0: 
        message.set("Game name must be longer!", False)
        return message        
    if len(test_game) != 0:
        message.set("Game already exists!", False)
        return message
    game = Game.objects.create(author=profile, name=name, description=description)
    start_dialog = create_new_dialog(name = "Start dialog", game = game)
    start_frame = create_new_frame(name = "Start frame", game = game, start_dialog = start_dialog)
    if (start_frame != None): game.save()
    return message

def create_new_frame(game, name, start_dialog = None):
    frame = Frame.objects.create(name = name, game = game, start_dialog = start_dialog)
    frame.save()
    return frame
    
def create_new_dialog(game, name):
    dialog = Dialog.objects.create(name = name, game = game)
    dialog.data.save(dialog.data.storage.get_available_name(game.author.user.username), ContentFile(""))
    dialog.save()
    return dialog
    
def set_start_dialog(game, index):
    game.dialogs[index], game.dialogs[0] = game.dialogs[0], game.dialogs[index]
    
