from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.validators import validate_email
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from .utils import find_new_user, Status_message, test_username, create_full_user, create_new_game, auth
from .models import Profile, Game, Frame, Item, Dialog, Gameplay
from django.shortcuts import redirect

def base(request):
    if request.method == "POST":
        if "LogOut" in request.POST:
            logout(request)   
            return HttpResponseRedirect("/login/")
        if "Home" in request.POST:
            return redirect("userPage", username = request.user.username)
    return None;

def loginPage(request):
    message = Status_message()
    if request.method == "POST":
        if "LogIn" in request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/user_page/"+username+"/")
            else:
                message.set("User does not exist.", False)
        if "SignUp" in request.POST:
            print("email")
            email = request.POST['email']
            try:
                validate_email(email)
                valid_email = True
            except validate_email.ValidationError:
                valid_email = False
            if (valid_email):
                if not User.objects.filter(email = email):
                    password = User.objects.make_random_password() + User.objects.make_random_password() + User.objects.make_random_password()
                    email_to_send = EmailMessage(
                        subject = 'Pixied Key',
                        body = "Hi, new Imp or Pixie! \r\r To enter PIXIED OpenWorld use this link: \r" +str(request.META['HTTP_HOST'])+"/init_page/"+password+" \r\rSee you soon!",
                        from_email = "MailPixie@openworld.cz",
                        to = [email],
                    )
                    try:
                        email_to_send.send(fail_silently = False)
                        message.set("Email has been sent to " + email, False) 
                        num_of_users = User.objects.count()
                        new_user = User.objects.create_user('new_user:' + str(num_of_users), email, password)
                    except:
                        message.set("An ERROR has appeard, please try it later", False)         
                else:
                    message.set("Email is already registered, please use another one", False)
            else:  
                message.set("You must fill in valid email", False)  
    context = {
        'message_text': message.value,
        'message_status': message.status,
    }
    return render(request, 'polls/login.html', context)

@login_required(login_url='/login/')
def userPage(request, username):
    message = Status_message()
    if not auth(request, username): return HttpResponseRedirect("/login/")
    base_result = base(request)
    if base_result: return base_result
    if request.method == "POST":
        if "Draw" in request.POST:
            return HttpResponseRedirect("/user_page/"+username+"/draw/")
        if "Edit" in request.POST:
            return HttpResponseRedirect("/user_page/"+username+"/edit/")
    user = User.objects.get(username=username);

    context = {
        "user": user,
        "rating": user.profile.get_rating(), 
        "num_followers": user.profile.followers.all().count(), 
    }
    return render(request, 'polls/userPage.html', context)

@login_required(login_url='/login/')
def drawPage(request, username):
    if not auth(request, username): return HttpResponseRedirect("/login/")
    base_result = base(request)
    if base_result: return base_result
    user = User.objects.get(username=username)

    context = {
        "user": user, 
    }
    return render(request, 'polls/drawPage.html', context)

@login_required(login_url='/login/')
def editPage(request, username):    
    base_result = base(request)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    user = User.objects.get(username=username)
    games = Game.objects.filter(author = user.profile)
    message = Status_message()
    if request.method == "POST":
        if "createNew" in request.POST:
            gamename = request.POST['name']
            description = request.POST['description']
            message = create_new_game(user.profile, gamename, description)
            if message.status:
                return HttpResponseRedirect("/user_page/"+username+"/edit/"+gamename)

    context = {
        "user": user,
        "games": games,
        "num_of_games": games.count(),
        'message_text': message.value,
        'message_status': message.status,
    }
    return render(request, 'polls/editPage.html', context)

@login_required(login_url='/login/')
def delete_game(request, username, gamename):
    base_result = base(request)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    game = Game.objects.get(author__user__username = username, name = gamename)
    game.delete()

    return redirect("editPage", username = username)

@login_required(login_url='/login/')
def editGamePage(request, username, gamename):
    base_result = base(request)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    user = User.objects.get(username=username)
    game = Game.objects.get(author = user.profile, name = gamename)
    frames = Frame.objects.filter(game = game)
    dialogs = Dialog.objects.filter(game = game)
    items = Item.objects.filter(game = game)

    if request.method == "POST":
        if "save-dialog" in request.POST:
            dialogname = request.POST['name']
            dialogtext = request.POST['text']
            Dialog.objects.get(game = game, name = dialogname).write(dialogtext)
        elif "delete-dialog" in request.POST:
            dialogname = request.POST['name']
            Dialog.objects.get(game = game, name = dialogname).delete()

    context = {
        "gamename": game.name,
        "frames": frames,
        "dialogs": dialogs,
        "items": items,
    }
    return render(request, 'polls/editGamePage.html', context)

@login_required(login_url='/login/')
def play(request, username, author ,gamename, load = False):
    base_result = base(request)
    message = Status_message("Game is ready.", True)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    
    try:
        user = User.objects.get(username=username)
        game = Game.objects.get(author__user__username = author, name = gamename)
    except:
        message = Status_message("Game doesnt exist.", False)
        return render(request, 'polls/playPage.html', {'message_text': message.value, 'message_status': message.status,})
        
    if request.method == "POST":
        if "saveGame" in request.POST:
            if not auth(request, username): return JsonResponse("", safe = False)
            rating = min(5, int(request.POST['stars'])) if username!=author else 0
            dialogname = request.POST['dialogname']
            sentence = int(request.POST['sentence'])
            flag_list = request.POST['flagList']
            item_list = request.POST['itemList']
            comment = request.POST['comment']
            gameplays = Gameplay.objects.filter(game = game, user = user)
            if gameplays.count() == 1:
                gameplays.update(
                    rating = rating,
                    dialog = Dialog.objects.get(game = game, name = dialogname),
                    sentence = sentence,
                    flag_list = flag_list,
                    item_list = item_list,
                    comment = comment,
                )
            else:
                Gameplay.objects.create(
                    game = game, 
                    user = user, 
                    rating = rating,
                    dialog = Dialog.objects.get(game = game, name = dialogname),
                    sentence = sentence,
                    flag_list = flag_list,
                    item_list = item_list,
                    comment = comment,
                ).save()
        return redirect("choose_for_play", username = request.user.username)
    
    try:
        gp = Gameplay.objects.get(game=game, user = user)
        rating = gp.rating
        comment = gp.comment
    except:
        comment = ""
        rating = 0
    if load:
        start_frame = gp.frame
        start_dialog = gp.dialog
        items = gp.item_list
        flags = gp.flag_list
        sentence = gp.sentence
    else:
        sentence = 0
        if (username == author) or (game.playable):
            try:
                start_frame = Frame.objects.filter(game = game)[0]
                start_dialog = Dialog.objects.filter(game = game)[0]
                items = "{}"
                flags = "[]"
            except:
                message = Status_message("Game is broken.", False)
                return render(request, 'polls/playPage.html', {'message_text': message.value, 'message_status': message.status,})   
        else:
            message = Status_message("Game is not playable.", False)
    context = {
        "game": game,
        "start_dialog": start_dialog,
        "sentence": sentence,
        "start_frame": start_frame,
        "item_list": items,
        "flag_list": flags,
        'message_text': message.value,
        'message_status': message.status,
        'rating': rating,
        'comment': comment,
        'load': load,
    }
    return render(request, 'polls/playPage.html', context)
    
@login_required(login_url='/login/')
def choose_for_play(request, username):
    base_result = base(request)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    user = User.objects.get(username=username)
    my_games = Game.objects.filter(author = user.profile, playable = True)
    their_games = Game.objects.filter(players = user, playable = True)
    games = []
    for g in my_games:
        try:
            Gameplay.objects.get(game=g, user = user)
            saved = True
        except:
            saved = False
        g.saved = saved
        games.append(g)
    for g in their_games:
        try:
            Gameplay.objects.get(game=g, user = user)
            saved = True
        except:
            saved = False
        g.saved = saved
        games.append(g)
    
    message = Status_message()

    context = {
        "user": user,
        "games": games,
        'message_text': message.value,
        'message_status': message.status,
    }
    return render(request, 'polls/choose_game.html', context)

@login_required(login_url='/login/')
def explore(request, username):
    base_result = base(request)
    message = Status_message("Ok", True)
    if not auth(request, username): return HttpResponseRedirect("/login/")
    if base_result: return base_result
    
    user = User.objects.get(username=username)
    my_games = user.profile.games.filter(playable=True)

    
    context = {
        'my_favourites': user.favourites.all(),
        'my_games': my_games,
        'message_text': message.value,
        'message_status': message.status,
    }
    return render(request, 'polls/explorer.html', context)

def init_page(request, key):
    
    message = Status_message()
    user = find_new_user(request, key)
    if user is None:
        return HttpResponseRedirect("/user_page/")

    if request.POST.get("SignUp"):
        username = request.POST['username']
        password = request.POST['password']
        passwordCh = request.POST['passwordCh']
        gender = request.POST['gender']

        if password == passwordCh:
            message = test_username(username)
            if message.status:
                create_full_user(user, username, password, gender)
                login(request, user)
                return HttpResponseRedirect("/user_page/"+username+"/")
        else: 
            message.set("Your verification password doesn't match.", False)
    context = {
        "user": user,
        'message_text': message.value,
        'message_status': message.status,
    }
    return render(request, 'polls/init_page.html', context)
